#define BLYNK_PRINT Serial


#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

#define TRIGGERPIN 5
#define ECHOPIN    4
#define led D3

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "799dfe5fd4664c6d95f60afcc1ddfb21";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Cloudy";
char pass[] = "142142100017";

WidgetLCD lcd(V1);

void setup()
{
  // Debug console
  Serial.begin(9600);
  pinMode(TRIGGERPIN, OUTPUT);
  pinMode(ECHOPIN, INPUT);
  pinMode(D3, OUTPUT);
  Blynk.begin(auth, ssid, pass);
  // You can also specify server:
  //Blynk.begin(auth, ssid, pass, "blynk-cloud.com", 8442);
  //Blynk.begin(auth, ssid, pass, IPAddress(192,168,1,100), 8442);

  lcd.clear(); //Use it to clear the LCD Widget
  lcd.print(0, 0, "Distance in cm"); // use: (position X: 0-15, position Y: 0-1, "Message you want to print")
  // Please use timed events when LCD printintg in void loop to avoid sending too many commands
  // It will cause a FLOOD Error, and connection will be dropped
}


void flood(){
  long duration, distance;
  digitalWrite(TRIGGERPIN, LOW);  
  delayMicroseconds(3); 
  
  digitalWrite(TRIGGERPIN, HIGH);
  delayMicroseconds(12); 
  
  digitalWrite(TRIGGERPIN, LOW);
  duration = pulseIn(ECHOPIN, HIGH);
  distance = (duration/2) / 29.1;
  Serial.print(distance);
  Serial.println(" Cm");

//  lcd.clear();
//  lcd.print(0, 0, "Distance in cm"); // use: (position X: 0-15, position Y: 0-1, "Message you want to print")
//  lcd.print(7, 1, distance);
  
  if (distance <= 10) {
    
    digitalWrite(D3, HIGH);
    
  }
  else {
    digitalWrite(D3,LOW);
  }

  lcd.clear();
  lcd.print(0, 0, "Distance in cm"); // use: (position X: 0-15, position Y: 0-1, "Message you want to print")
  lcd.print(7, 1, distance);
  

}

void loop()
{
  Blynk.run();
  flood();
  delay(1000);

}
