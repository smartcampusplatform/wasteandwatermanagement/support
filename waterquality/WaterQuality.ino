
float threshold = 4.0;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int sensorValue = analogRead(A2);
  float voltage = sensorValue * (5.0/1024.0);
  // Check to see if the the circuit was connected or broken
  delay(100); 
  Serial.println(voltage);
  if (voltage > threshold) {
    
    Serial.println("Not Ok");
  } else {
    Serial.println("Ok");
  }
}
